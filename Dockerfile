FROM openjdk:8-jdk-alpine
EXPOSE 8090
WORKDIR /app
ADD target/cms-degree-service.jar cms-degree-service.jar
ENTRYPOINT ["java", "-jar", "/app/cms-degree-service.jar"]