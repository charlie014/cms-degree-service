package cms.degree.service.service;

import cms.degree.service.exception.DegreeExistException;
import cms.degree.service.exception.DegreeNotFoundException;
import cms.degree.service.exception.EmptyListOfDegreeException;
import cms.degree.service.model.Degree;
import cms.degree.service.repository.DegreeRepository;
import com.sun.istack.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class DegreeService {

    private final DegreeRepository degreeRepository;
    private static Logger logger = LoggerFactory.getLogger(DegreeService.class);

    @Autowired
    public DegreeService(DegreeRepository degreeRepository) {
        this.degreeRepository = degreeRepository;
    }

    @Transactional
    public List<Degree> retrieveAllDegrees() throws EmptyListOfDegreeException {

        List<Degree> response = degreeRepository.findAll();

        if( response.isEmpty()) {
            throw new EmptyListOfDegreeException("Empty List of degrees");
        } else {
            return response;
        }
    }

    @Transactional
    public Degree retrieveDegreeByCode(String degreeCode) throws Exception {
        Optional<Degree> response = degreeRepository.retrieveDegreeByCode(degreeCode);
        if (!response.isPresent()) {
            throw new DegreeNotFoundException("Unable to find degree code " + degreeCode);
        } else {
            return response.get();
        }
    }

    @Transactional
    public Degree insertNewDegree(@NotNull Degree newDegree) {
        Optional<Degree> response = degreeRepository.retrieveDegreeByCode(newDegree.getDegreeCode());
        if (!response.isPresent()) {
            return degreeRepository.save(newDegree);
        } else {
            throw new DegreeExistException("Degree " + newDegree.getDegreeCode() + " is already exist");
        }
    }
}
