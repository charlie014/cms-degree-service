package cms.degree.service.exception;

import javassist.NotFoundException;

public class EmptyListOfDegreeException extends NotFoundException {

    public EmptyListOfDegreeException(String msg) {
        super(msg);
    }
}
