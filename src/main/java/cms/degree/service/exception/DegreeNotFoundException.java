package cms.degree.service.exception;

import javassist.NotFoundException;

public class DegreeNotFoundException extends NotFoundException {
        private static final long serialVersionUID = 1L;

        public DegreeNotFoundException(String msg) {
            super(msg);
        }
}
