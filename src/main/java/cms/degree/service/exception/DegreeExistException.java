package cms.degree.service.exception;

public class DegreeExistException extends RuntimeException {
    public DegreeExistException(String message) {
        super(message);
    }
}
