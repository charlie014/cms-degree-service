package cms.degree.service.exception;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {DegreeNotFoundException.class, EmptyListOfDegreeException.class})
    public ResponseEntity<ErrorException> handleNotFoundException(NotFoundException e) {

        ErrorException errorException = new ErrorException("NOT_FOUND_ERROR", e.getMessage());
        errorException.setTimestamp(LocalDateTime.now());
        errorException.setStatus(HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errorException, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {DegreeCodeBlankException.class})
    public ResponseEntity<ErrorException> handleBadRequestErrorException(Exception e) {

        ErrorException errorException = new ErrorException("ILLEGAL ARGUMENTS", e.getMessage());
        errorException.setTimestamp(LocalDateTime.now());
        errorException.setStatus(HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(errorException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {DegreeExistException.class})
    public ResponseEntity<ErrorException> handleInterErrorException(RuntimeException e) {

        ErrorException errorException = new ErrorException("INTERNAL_SERVER_ERROR", e.getMessage());
        errorException.setTimestamp(LocalDateTime.now());
        errorException.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ResponseEntity<>(errorException, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
