package cms.degree.service.exception;

public class DegreeCodeBlankException extends IllegalArgumentException {
    private static final long serialVersionUID = 1L;

    public DegreeCodeBlankException(String msg) {
        super(msg);
    }
}
