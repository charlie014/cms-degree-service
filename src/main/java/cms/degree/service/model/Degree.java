package cms.degree.service.model;

import javax.persistence.*;

@Entity
@Table(name = "degrees")
public class Degree {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "degree_code")
    private String degreeCode;

    @Column(name = "degree_title")
    private String degreeTitle;

    @Column(name = "degree_type")
    private String type;

    private String description;

    private String campus;

    @Column(name = "lowest_score")
    private Integer lowestScore;

    @Column(name = "guaranteed_score")
    private Integer guaranteedScore;

    @Column(name = "lowest_atar")
    private Integer lowestAtar;

    public Degree() {
    }

    public Degree(Integer id, String degreeCode, String degreeTitle, String type, String description, String campus,
                  Integer lowestScore, Integer guaranteedScore, Integer lowestAtar) {
        this.id = id;
        this.degreeCode = degreeCode;
        this.degreeTitle = degreeTitle;
        this.type = type;
        this.description = description;
        this.campus = campus;
        this.lowestScore = lowestScore;
        this.guaranteedScore = guaranteedScore;
        this.lowestAtar = lowestAtar;
    }

    public Degree(String degreeCode, String degreeTitle, String type, String description, String campus,
                  Integer lowestScore, Integer guaranteedScore, Integer lowestAtar) {
        this.degreeCode = degreeCode;
        this.degreeTitle = degreeTitle;
        this.type = type;
        this.description = description;
        this.campus = campus;
        this.lowestScore = lowestScore;
        this.guaranteedScore = guaranteedScore;
        this.lowestAtar = lowestAtar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDegreeCode() {
        return degreeCode;
    }

    public void setDegreeCode(String degreeCode) {
        this.degreeCode = degreeCode;
    }

    public String getDegreeTitle() {
        return degreeTitle;
    }

    public void setDegreeTitle(String degreeTitle) {
        this.degreeTitle = degreeTitle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public Integer getLowestScore() {
        return lowestScore;
    }

    public void setLowestScore(Integer lowestScore) {
        this.lowestScore = lowestScore;
    }

    public Integer getGuaranteedScore() {
        return guaranteedScore;
    }

    public void setGuaranteedScore(Integer guaranteedScore) {
        this.guaranteedScore = guaranteedScore;
    }

    public Integer getLowestAtar() {
        return lowestAtar;
    }

    public void setLowestAtar(Integer lowestAtar) {
        this.lowestAtar = lowestAtar;
    }
}
