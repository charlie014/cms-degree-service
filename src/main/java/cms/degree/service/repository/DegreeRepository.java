package cms.degree.service.repository;

import cms.degree.service.model.Degree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface DegreeRepository extends JpaRepository<Degree, Integer> {

    @Query(value = "SELECT * FROM degrees WHERE degree_code = :degreeCode", nativeQuery = true)
    Optional<Degree> retrieveDegreeByCode(String degreeCode);
}
