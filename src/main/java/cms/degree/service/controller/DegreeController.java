package cms.degree.service.controller;

import cms.degree.service.exception.EmptyListOfDegreeException;
import cms.degree.service.model.Degree;
import cms.degree.service.service.DegreeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Api(tags = "Degree Service")
@RequestMapping("/api/service")
public class DegreeController {

    private final DegreeService degreeService;

    @Autowired
    public DegreeController(DegreeService degreeService) {
        this.degreeService = degreeService;
    }

    @ApiOperation(
            value = "Retrieve degree details",
            notes = "Retrieve list of degrees",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Retrieve list of degrees successfully"),
            @ApiResponse(code = 404, message = "Country not found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, value = "/alldegree")
    @ResponseBody
    public List<Degree> retrieveAllDegrees() throws EmptyListOfDegreeException {
        return degreeService.retrieveAllDegrees();
    }

    @ApiOperation(
            value = "Retrieve degree by code",
            notes = "Retrieve an existing degree by code",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Retrieve degree by code successfully"),
            @ApiResponse(code = 404, message = "Degree not found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, value = "/degree/{degreeCode}")
    @ResponseBody
    public Degree retrieveDegreeByCode(
            @ApiParam(name = "degreeCode", required = true, value = "The degree code")
            @PathVariable("degreeCode") String degreeCode
    ) throws Exception {
        return degreeService.retrieveDegreeByCode(degreeCode);
    }

    @ApiOperation(value = "Create new degree", notes = "Create a new degree", produces = APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Create new degree successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/degree/create", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Degree insertNewDegree(
            @ApiParam(required = true)
            @RequestBody Degree degree) {
        return degreeService.insertNewDegree(degree);
    }
}
