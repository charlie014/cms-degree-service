package cms.degree.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class CmsDegreeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CmsDegreeServiceApplication.class, args);
	}

}
