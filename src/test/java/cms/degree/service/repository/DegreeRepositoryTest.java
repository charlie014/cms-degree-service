package cms.degree.service.repository;

import cms.degree.service.model.Degree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestPropertySource(locations = "classpath:test.properties")
public class DegreeRepositoryTest {

    @Autowired
    private DegreeRepository degreeRepository;


    @Test
    public void retrieveAllDegrees() {
        Degree degree = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78
        );

        degreeRepository.save(degree);
        List<Degree> expectedResponse = degreeRepository.findAll();

        Assertions.assertTrue(expectedResponse.size() > 0);
    }

    @Test
    public void retrieveDegreeByCode() {
        Degree degree = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78
        );

        degreeRepository.save(degree);
        Optional<Degree> expectedResponse = degreeRepository.retrieveDegreeByCode("C2000");
        Assertions.assertEquals(expectedResponse, Optional.of(degree));
    }

    @Test
    public void shouldInsertNewDegree() {
        Degree newDegree = new Degree(
                "C2001",
                "Bachelor of Computer Science",
                "Bachelor",
                "Bachelor for CS students",
                "Clayton",
                85,
                80,
                79
        );
        Degree actual = degreeRepository.save(newDegree);
        List<Degree> expectedList = degreeRepository.findAll();

        Assertions.assertNotNull(actual);
        Assertions.assertTrue(expectedList.size() > 0);
    }
}