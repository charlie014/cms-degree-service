package cms.degree.service.controller;

import cms.degree.service.exception.DegreeExistException;
import cms.degree.service.exception.DegreeNotFoundException;
import cms.degree.service.exception.EmptyListOfDegreeException;
import cms.degree.service.model.Degree;
import cms.degree.service.service.DegreeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DegreeController.class)
public class DegreeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    @MockBean
    private DegreeService degreeService;

    @Test
    public void shouldRetrieveListOfDegree() throws Exception {
        List<Degree> expected = new ArrayList<>();
        expected.add(new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78
        ));
        Mockito.when(degreeService.retrieveAllDegrees())
                .thenReturn(expected);

        String url = "/api/service/alldegree";
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponse = response.getResponse().getContentAsString();

        String expectedJsonResponse = mapper.writeValueAsString(expected);
        Assertions.assertEquals(actualResponse, expectedJsonResponse);
    }

    @Test
    public void shouldThrowExceptionWhenReturnEmptyListOfDegree() throws Exception {
        Mockito.when(degreeService.retrieveAllDegrees())
                .thenThrow(EmptyListOfDegreeException.class);
        String url = "/api/service/degree/";
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldRetrieveDegreeByCode() throws Exception {
        Degree degree = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78);
        Mockito.when(degreeService.retrieveDegreeByCode(degree.getDegreeCode()))
                .thenReturn(degree);
        String url = "/api/service/degree/C2000";
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponse = response.getResponse().getContentAsString();
        String expectedJsonResponse = mapper.writeValueAsString(degree);
        Assertions.assertEquals(actualResponse, expectedJsonResponse);
    }

    @Test
    public void shouldReturn404WhenDegreeCodeIsEmpty() throws Exception {
        Mockito.when(degreeService.retrieveDegreeByCode(""))
                .thenThrow(DegreeNotFoundException.class);
        String url = "/api/service/degree/";
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturn404WhenDegreeCodeIsInvalid() throws Exception {
        Mockito.when(degreeService.retrieveDegreeByCode("C20000"))
                .thenThrow(DegreeNotFoundException.class);
        String url = "/api/service/degree/C20000";
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldInsertNewDegree() throws Exception{
        String url = "/api/service/degree/create";
        Degree newDegree = new Degree(
                "C2001",
                "Bachelor of Computer Science",
                "Bachelor",
                "Bachelor for CS students",
                "Clayton",
                85,
                80,
                79
        );
        Mockito.when(degreeService.insertNewDegree(any(Degree.class)))
                .thenReturn(new Degree(
                        "C2001",
                        "Bachelor of Computer Science",
                        "Bachelor",
                        "Bachelor for CS students",
                        "Clayton",
                        85,
                        80,
                        79
                ));
        mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .content(asJsonString(newDegree))
                .contentType(APPLICATION_JSON_VALUE)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.degreeCode").value(newDegree.getDegreeCode()));
    }

    @Test
    public void shouldThrowExceptionWhenInsertExistingDegree() throws Exception {
        String url = "/api/service/degree/create";
        Degree newDegree = new Degree(
                "C2001",
                "Bachelor of Computer Science",
                "Bachelor",
                "Bachelor for CS students",
                "Clayton",
                85,
                80,
                79
        );
        Mockito.when(degreeService.insertNewDegree(any(Degree.class)))
                .thenThrow(DegreeExistException.class);
        mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .content(asJsonString(newDegree))
                .contentType(APPLICATION_JSON_VALUE)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().is5xxServerError());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
