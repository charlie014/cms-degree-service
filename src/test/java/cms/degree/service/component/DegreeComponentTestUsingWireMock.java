package cms.degree.service.component;

import cms.degree.service.controller.DegreeController;
import cms.degree.service.exception.EmptyListOfDegreeException;
import cms.degree.service.model.Degree;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@SpringBootTest
public class DegreeComponentTestUsingWireMock {

    @Autowired
    private DegreeController degreeController;

    private WireMockServer wireMockServer;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        wireMockServer = new WireMockServer();
        configureFor("localhost", 8080);
        wireMockServer.start();
    }

    @Test
    public void shouldReturnListOfDegree() throws EmptyListOfDegreeException {
        stubFor(get(urlPathEqualTo("/api/alldegree"))
                .willReturn(okJson("" +
                        "[{" +
                        " \"id\": \"1\"," +
                        " \"degreeCode\": \"C2000\"," +
                        " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                        " \"type\": \"Bachelor\"," +
                        " \"description\": \"Bachelor for IT students\"," +
                        " \"campus\": \"Clayton\"," +
                        "  \"lowestScore\": \"80\"," +
                        " \"guaranteedScore\": \"75\"," +
                        " \"lowestAtar\": \"79\"" +
                        "}]")));
        List<Degree> actual = degreeController.retrieveAllDegrees();
        Assertions.assertEquals(6, actual.size());
    }

    @Test
    public void shouldReturnDegreeDetails() throws Exception {
        stubFor(get(urlPathEqualTo("/api/degree/C2000"))
            .willReturn(okJson(
                    "{" +
                            " \"id\": \"1\"," +
                            " \"degreeCode\": \"C2000\"," +
                            " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                            " \"type\": \"Bachelor\"," +
                            " \"description\": \"Bachelor for IT students\"," +
                            " \"campus\": \"Clayton\"," +
                            "  \"lowestScore\": \"80\"," +
                            " \"guaranteedScore\": \"79\"," +
                            " \"lowestAtar\": \"75\"" +
                            "}")));
        Degree actual = degreeController.retrieveDegreeByCode("C2000");
        Degree expected = new Degree(
                1,
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                79,
                75
        );
        Assertions.assertEquals(mapper.writeValueAsString(expected), mapper.writeValueAsString(actual));
    }
}
