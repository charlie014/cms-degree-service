package cms.degree.service.component;

import cms.degree.service.model.Degree;
import org.junit.Ignore;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Disabled
@Ignore
@SpringBootTest
@AutoConfigureMockMvc
public class DegreeComponentTest {

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void shouldReturn200WhenSuccessfulRetrieveDegreeList() throws Exception {
        String url = "/api/alldegree";
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldThrowExceptionWhenEnterInvalidURL() throws Exception {
        String url = "/api/alldegre";
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Disabled
    public void shouldReturn200WhenSuccessfulRetrieveDegreeDetails() throws Exception{
        String url = "/api/degree/C2000";
        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturn404WhenDegreeCodeIsEmpty() throws Exception {
        String url = "/api/degree/";

        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Disabled
    public void shouldReturn200WhenSuccessfulInsertNewDegree() throws Exception {
        String url = "/api/degree/create";
        Degree degree = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78
        );

        mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contentType("application/json")
                .content(mapper.writeValueAsString(degree)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @Disabled
    public void shouldThrow500ExceptionWhenInsertingExistingDegree() throws Exception {
        String url = "/api/degree/create";
        Degree degree = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78
        );

        mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contentType("application/json")
                .content(mapper.writeValueAsString(degree)))
                .andExpect(status().is5xxServerError());
    }
}
