package cms.degree.service.service;

import cms.degree.service.exception.DegreeExistException;
import cms.degree.service.exception.DegreeNotFoundException;
import cms.degree.service.exception.EmptyListOfDegreeException;
import cms.degree.service.model.Degree;
import cms.degree.service.repository.DegreeRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class DegreeServiceTest {

    @Mock
    private DegreeRepository degreeRepository;

    @InjectMocks
    private DegreeService degreeService;

    @Test
    public void shouldRetrieveAllDegree() throws EmptyListOfDegreeException {
        List<Degree> expectedListOfDegree = new ArrayList<>();
        expectedListOfDegree.add(new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78));
        expectedListOfDegree.add(new Degree(
                "C2001",
                "Bachelor of Computer Science",
                "Bachelor",
                "Bachelor for CS students",
                "Clayton",
                85,
                79,
                80
        ));
        Mockito.when(degreeRepository.findAll())
                .thenReturn(expectedListOfDegree);
        List<Degree> actual = degreeService.retrieveAllDegrees();
        Assertions.assertEquals(expectedListOfDegree, actual);
    }

    @Test
    public void shouldThrowExceptionWhenReturnEmptyListOfDegree() {
        Mockito.when(degreeRepository.findAll())
                .thenReturn(new ArrayList<>());
        Exception thrown = Assertions.assertThrows(EmptyListOfDegreeException.class, () ->
                degreeService.retrieveAllDegrees()
        );
        Assertions.assertTrue(thrown.getMessage().contains("Empty List of degrees"));
    }

    @Test
    public void retrieveDegreeByCode() throws Exception {
        Degree degree = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78);
        Mockito.when(degreeRepository.retrieveDegreeByCode("C2000"))
                .thenReturn(Optional.of(degree));
        Degree actual = degreeService.retrieveDegreeByCode(degree.getDegreeCode());
        Assertions.assertEquals(degree, actual);
    }

    @Test
    public void shouldThrowAnExceptionWhenDegreeCodeIsEmpty() {
        Mockito.when(degreeRepository.retrieveDegreeByCode("C200000"))
                .thenReturn(Optional.empty());
        Exception thrown = Assertions.assertThrows(DegreeNotFoundException.class, () ->
                degreeService.retrieveDegreeByCode("C200000"));
        Assertions.assertTrue(thrown.getMessage().equalsIgnoreCase("Unable to find degree code C200000"));
    }

    @Test
    public void shouldInsertNewDegree() {
        Degree newDegree = new Degree(
                "C2001",
                "Bachelor of Computer Science",
                "Bachelor",
                "Bachelor for CS students",
                "Clayton",
                85,
                80,
                79
        );
        Mockito.when(degreeRepository.save(newDegree))
                .thenReturn(new Degree(
                        "C2001",
                        "Bachelor of Computer Science",
                        "Bachelor",
                        "Bachelor for CS students",
                        "Clayton",
                        85,
                        80,
                        79
                ));
        Degree expected = degreeService.insertNewDegree(newDegree);
        Assertions.assertEquals(expected.getCampus(), "Clayton");
        Assertions.assertEquals(expected.getDegreeCode(), "C2001");
    }

    @Test
    public void shouldThrowExceptionWhenDegreeAlreadyExist() {
        Degree newDegree = new Degree(
                "C2001",
                "Bachelor of Computer Science",
                "Bachelor",
                "Bachelor for CS students",
                "Clayton",
                85,
                80,
                79
        );
        Mockito.when(degreeRepository.retrieveDegreeByCode("C2001"))
                .thenReturn(Optional.of(new Degree(
                        "C2001",
                        "Bachelor of Computer Science",
                        "Bachelor",
                        "Bachelor for CS students",
                        "Clayton",
                        85,
                        80,
                        79
                )));
        Exception thrown = Assertions.assertThrows(DegreeExistException.class, () ->
                degreeService.insertNewDegree(newDegree)
        );
        Assertions.assertTrue(thrown.getMessage().contains("Degree C2001 is already exist"));
    }
}
