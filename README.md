## Build
Build the project 
```
mvn clean install
```

## Dockerize the database and run it in container
```
docker run --name mysql-degree-service -p 3307:3306 -e MYSQL_ROOT_PASSWORD=degreewebservice -e MYSQL_DATABASE=testing -e MYSQL_USER=ds -e MYSQL_PASSWORD=degreewebservice -d mysql:5.6
```

## Control the database in terminal
```
docker exec -it mysql-degree-service bash
mysql -u root --pdegreewebservice
```

```
create table degrees (
    -> id int not null,
    -> degree_code text,
    -> degree_title text,
    -> degree_type text,
    -> description text,
    -> campus text,
    -> lowest_score int,
    -> guaranteed_score int,
    -> lowest_atar int,
    -> primary key (id)
    -> );
```

```
insert into degrees (id, degree_code, degree_title, degree_type, description, campus, lowest_score, guaranteed_score, lowest_atar) values (1, 'C2000', 'Bachelor of Information Technology', 'Bachelor', 'Bachelor for IT students', 'Clayton', 80, 79, 75);
insert into degrees (id, degree_code, degree_title, degree_type, description, campus, lowest_score, guaranteed_score, lowest_atar) values (2, 'C2001', 'Bachelor of Computer Science', 'Bachelor', 'Bachelor for CS students', 'Clayton', 85, 80, 79);
```
## Dockerization service

### Build
```
docker build -t cms-degree-service:1.0 .
```

### Run
```
docker run -p 8090:8090 --name cms-degree-service --link mysql-degree-service:mysql -d cms-degree-service:1.0
```



